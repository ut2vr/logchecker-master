import setuptools
from logchecker.version import __version__

setuptools.setup(
    name="LogChecker",  # под этим именем будет зарегистрирован пакет pip
    version=__version__,
    author="Pavlo Mishchenko, ut2vr",
    author_email="ut2vr.pm@gmail.com",
    description="GUI for ham radio log cross checker",
    url="https://bitbucket.org/ut2vr/logchecker-master/src/master/",
    license="GNU GPLv3",
    packages=["logchecker", "logXchecker"],     # директории будут помещены в ./local/lib/
    package_data={
        "logchecker": [                         # что будет размещено помимо файлов *.py
            "*.ui",
            "README.rst",
            "LICENSE",
            "Languages/*.qm",
            "Languages/*.ts",
            "Contests/VHF Sumy Open 2020/VHF Sumy Open 2020.rules",
            "Contests/VHF Sumy Open 2020/checklogs/*",
            "Contests/VHF Sumy Open 2020/logs/*",
        ],
        "logXchecker": [
            "locale/ru_RU/LC_MESSAGES/*",
            "locale/ua_UA/LC_MESSAGES/*",
            "locale/*.pot",
        ],
    },
    data_files=[
        ("share/applications", ["logchecker.desktop"]),     # помещается в ./local/share/applications
        ("share/pixmaps", ["logchecker.png"]),              # помещается в ./local/share/pixmaps
    ],
    scripts=[
        "set_contests.sh"                           # будет помещен в ./local/bin
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Environment :: X11 Applications :: Qt",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Topic :: Communications :: Ham Radio",
    ],
    entry_points={
        "gui_scripts":
            ["logchecker = logchecker.__main__:main"]   # имя скрипта запуска в ./local/bin
    },
    python_requires='>=3.6',
    install_requires=[
        "PyQt5>=5.14.2",
        "coverage==4.2",
        "dicttoxml==1.7.4",
        "nose==1.3.7",
        "validate-email==1.3"]
)


