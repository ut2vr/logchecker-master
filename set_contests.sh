echo Setup folders
cd ~/.local/lib/python3.8/site-packages
chmod u+x logXchecker/logXchecker.py
if ! [ -d ~/Contests ]
	then
		mkdir ~/Contests
		echo Contests folder created
		mv logchecker/Contests ~/
    echo All subfolders from Contests relocated
fi
rm ~/.local/bin/set_contests.sh
echo Completed
