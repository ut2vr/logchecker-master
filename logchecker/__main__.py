#!/usr/bin/python3
# coding:utf-8

import sys
import os
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5.QtWidgets import QFileDialog
from logchecker.GUI import Ui_MainWindow
import configparser
from pathlib import Path
import subprocess
from PyQt5.QtCore import QTranslator
from functools import partial

settings = QtCore.QSettings('ut2vr', 'LogChecker')
language = 'en_EN'
CONTEST_PATH = ''
RULES_FILE_NAME = ''
checker = 'logXchecker.py'
checker_path = os.path.join(os.path.split(os.path.abspath(__file__))[0], '../logXchecker/{0}'.format(checker))
rules = configparser.ConfigParser()
command = [checker_path, ]
extension = '.txt'


class mywindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.statusBar()
        self.ui.actionOpen_contest_folder.triggered.connect(self.contest_select)
        self.ui.actionCompliance_check.triggered.connect(self.compliance_check)
        self.ui.actionCross_check_contest.triggered.connect(self.contest_check)
        self.ui.actionOpen_log.triggered.connect(self.file_check)

        self.ui.actionEn.triggered.connect(partial(self.select_language, 'en_EN'))
        self.ui.actionUa.triggered.connect(partial(self.select_language, 'ua_UA'))
        self.ui.actionRu.triggered.connect(partial(self.select_language, 'ru_RU'))

        self.ui.actionCreate_new_contest.triggered.connect(self.create_new_contest)
        self.ui.actionRestore_the_contest.triggered.connect(self.restore_contest)
        self.ui.actionArchive_the_contest.triggered.connect(self.archive_contest)
        self.ui.actionOpen_rules.triggered.connect(self.open_rules)
        self.ui.actionRead_me.triggered.connect(self.read_me)
        self.ui.actionAbout.triggered.connect(self.about)
        self.ui.pbclear.clicked.connect(self.clear_logwin)
        self.ui.pbsave.clicked.connect(self.pb_save_report)

        self.ui.rbtxt.clicked.connect(self.select_out_format)
        self.ui.rbjson.clicked.connect(self.select_out_format)
        self.ui.rbxml.clicked.connect(self.select_out_format)
        self.ui.rbcsv.clicked.connect(self.select_out_format)

        self.ui.rbedi.clicked.connect(self.rbedi_change)
        self.ui.rbrules.clicked.connect(self.rbrules_change)
        self.restore_geometry()
        if not os.path.exists(checker_path):
            self.ui.statusBar.showMessage(self.tr('"{0}" file not found'.format(checker)))
            self.ui.actionCompliance_check.setEnabled(False)
            self.ui.actionCross_check_contest.setEnabled(False)
            self.ui.actionOpen_log.setEnabled(False)

    def closeEvent(self, event):
        settings.setValue("geometry", self.saveGeometry())
        self.save_config()

    def save_config(self):
        settings.setValue("language", language)
        settings.setValue("Contest_path", CONTEST_PATH)
        settings.setValue("geometry", self.saveGeometry())

    def restore_geometry(self):
        if settings.value("geometry"):
            self.restoreGeometry(settings.value("geometry"))

    @QtCore.pyqtSlot()
    def rbedi_change(self):
        if self.ui.rbedi.isChecked():
            self.ui.rbcsv.setEnabled(True)
            self.ui.plainTextEdit.clear()

    @QtCore.pyqtSlot()
    def rbrules_change(self):
        if self.ui.rbrules.isChecked():
            self.ui.rbcsv.setEnabled(False)
            self.ui.plainTextEdit.clear()

    @QtCore.pyqtSlot()
    def open_rules(self):
        selected_rules_file, _ = QFileDialog.getOpenFileName(self, self.tr("Select rules file:"),
                                                             CONTEST_PATH, "(*.rules)",
                                                             options=QFileDialog.DontUseNativeDialog)
        if selected_rules_file == '':
            self.ui.statusBar.showMessage(self.tr('Rules file not selected'))
            return
        self.ui.plainTextEdit.clear()
        with open(selected_rules_file, 'r') as f:
            self.ui.plainTextEdit.appendPlainText(f.read())
        self.set_cursor(1)

    @QtCore.pyqtSlot()
    def select_language(self, lang):
        global language
        lang_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Languages")
        lang_file = "{}.qm".format(lang)
        if lang != 'en_EN':
            if not os.path.isfile(os.path.join(lang_path, lang_file)):
                self.ui.statusBar.showMessage(self._tr('Localization file {0} not found'.format(lang_file)))
                language = 'en_EN'
                return
        settings.setValue("language", lang)
        language = lang
        if lang == 'en_EN':
            self.ui.statusBar.showMessage('The change will take effect after restarting the application')
        if lang == 'ru_RU':
            self.ui.statusBar.showMessage('Изменение вступит в силу после перезапуска приложения')
        if lang == 'ua_UA':
            self.ui.statusBar.showMessage('Зміна набуде чинності після перезапуску програми')

    @QtCore.pyqtSlot()
    def create_new_contest(self):
        self.ui.statusBar.showMessage(self.tr('Not yet implemented'))

    @QtCore.pyqtSlot()
    def restore_contest(self):
        self.ui.statusBar.showMessage(self.tr('Not yet implemented'))

    @QtCore.pyqtSlot()
    def archive_contest(self):
        self.ui.statusBar.showMessage(self.tr('Not yet implemented'))

    @QtCore.pyqtSlot()
    def read_me(self):
        fn = os.path.join(os.path.split(os.path.abspath(__file__))[0], 'README.rst')
        if not os.path.exists(fn):
            self.ui.statusBar.showMessage('"README.rst" File not found')
        self.clear_logwin()
        self.set_cursor(1)
        with open(fn, 'r') as f:
            self.ui.plainTextEdit.appendPlainText(f.read())
        self.set_cursor(1)

    @QtCore.pyqtSlot()
    def about(self):
        self.ui.statusBar.showMessage(self.tr('GUI for logXchecker by ut2vr 2021'))

    def select_out_format(self):
        global extension, command
        if not self.ui.rbtxt.isChecked():
            self.ui.plainTextEdit.clear()
            if self.ui.rbjson.isChecked():
                command.append('-ojson')
                extension = '.json'
            if self.ui.rbxml.isChecked():
                command.append('-oxml')
                extension = '.XML'
            if self.ui.rbcsv.isChecked():
                command.append('-ocsv')
                extension = '.csv'
        else:
            extension = '.txt'
        return command

    def select_criteria(self, comm):
        if self.ui.rbedi.isChecked():
            comm.append('-fedi')
            self.ui.groupBox_3.setTitle(self.tr('edi format compliance check:'))
        else:
            comm.append('-r{0}'.format(RULES_FILE_NAME))
            self.ui.groupBox_3.setTitle(self.tr('rules format compliance check:'))
        return comm

    def contest_select(self):
        global RULES_FILE_NAME, CONTEST_PATH, rules
        self.clear_field()
        selected_folder = QFileDialog.getExistingDirectory(None, self.tr('Select contest folder:'), CONTEST_PATH,
                                                           options=QFileDialog.DontUseNativeDialog | QFileDialog.
                                                           ShowDirsOnly)
        if selected_folder == '':
            self.ui.statusBar.showMessage(self.tr('Folder not selected'))
            return
        CONTEST_PATH = selected_folder
        _, contest_name = os.path.split(CONTEST_PATH)
        RULES_FILE_NAME = os.path.join(CONTEST_PATH, contest_name + '.rules')
        if not os.path.exists(RULES_FILE_NAME):
            self.ui.statusBar.showMessage(self.tr('Contest folder does not contain a file with the correct test name'))
            return
        rules.read(RULES_FILE_NAME)
        if rules.get("contest", "name") != contest_name:
            self.ui.statusBar.showMessage(self.tr('Folder name and contest name in rules file do not match'))
            return
        self.ui.lecontest.setText(contest_name)
        self.ui.statusBar.showMessage(self.tr("Contest's file ready to check"))
        self.ui.rbrules.setEnabled(True)
        # TODO: проверить и исправить кодировку логов

    def contest_check(self):
        global command
        self.clear_logwin()
        if not self.exist_rules_check():
            return
        self.ui.statusBar.showMessage(self.tr('Please, wait...'))
        self.ui.statusBar.repaint()
        self.ui.groupBox_3.setTitle(self.tr('logs processed - ') + '{0}'.format(self.count()))
        self.ui.plainTextEdit.appendPlainText('\n')
        command = [checker_path, '-cc']
        logs_folder = os.path.join(CONTEST_PATH, 'logs')
        command.append(logs_folder)
        checklogs_folsder = os.path.join(CONTEST_PATH, 'checklogs')
        command.append('-cl')
        command.append(checklogs_folsder)
        command.append('-r{0}'.format(RULES_FILE_NAME))
        command = self.select_out_format()
        self.proc_start(command)
        self.save_report(os.path.join(CONTEST_PATH, 'Score'))
        command.append('-v')
        self.proc_start(command)
        self.save_report(os.path.join(CONTEST_PATH, 'UBN'))
        self.ui.statusBar.showMessage(self.tr('Score and UBN files are saved in the contest folder'))

    def exist_rules_check(self):
        if not os.path.exists(RULES_FILE_NAME):
            self.ui.statusBar.showMessage(self.tr('First define the folder and contest rules'))
            return False
        else:
            return True

    def compliance_check(self):
        global command
        self.ui.statusBar.showMessage(self.tr('Please, wait...'))
        self.ui.statusBar.repaint()
        self.ui.plainTextEdit.clear()
        command = [checker_path, '-mlc']
        selected_folder = QFileDialog.getExistingDirectory(None, self.tr('Select log folder:'), CONTEST_PATH,
                                                           options=QFileDialog.DontUseNativeDialog | QFileDialog.
                                                           ShowDirsOnly)
        if selected_folder == '':
            self.ui.statusBar.showMessage(self.tr('Folder with logs not selected'))
            return
        command.append(selected_folder)
        command = self.select_criteria(command)
        self.ui.plainTextEdit.appendPlainText('\n')
        command = self.select_out_format()
        self.proc_start(command)
        if self.ui.rbrules.isChecked():
            self.who_ruled()
        else:
            self.ui.statusBar.showMessage('')

    def file_check(self):
        global command
        self.ui.statusBar.showMessage('')
        self.ui.groupBox_3.setTitle('')
        self.ui.groupBox_3.repaint()
        if self.ui.rbrules.isChecked():
            if not self.exist_rules_check():
                return
        command = [checker_path, '-slc']
        selected_adi_file, _ = QFileDialog.getOpenFileName(self, self.tr("Select log file:"),
                                                           CONTEST_PATH, "(*.edi)",
                                                           options=QFileDialog.DontUseNativeDialog)
        command.append(selected_adi_file)
        _, filename = os.path.split(selected_adi_file)
        command = self.select_criteria(command)
        self.ui.plainTextEdit.clear()
        command = self.select_out_format()
        self.proc_start(command)
        if self.ui.rbrules.isChecked():
            self.who_ruled()

    def who_ruled(self):
        _, contest_name = os.path.split(CONTEST_PATH)
        self.ui.statusBar.showMessage(self.tr('Rules in force - ') + '{0}'
                                      .format(contest_name))

    def proc_start(self, comm):
        comm.append('-l{}'.format(language))
        process = subprocess.Popen(
            comm, stdout=subprocess.PIPE)
        data, _ = process.communicate()
        self.ui.plainTextEdit.appendPlainText(data.decode())
        self.set_cursor(1)

    def set_cursor(self, pos):
        if not self.ui.plainTextEdit.toPlainText() == '':
            cursor = self.ui.plainTextEdit.textCursor()
            cursor.setPosition(pos)
            self.ui.plainTextEdit.setTextCursor(cursor)

    @QtCore.pyqtSlot()
    def clear_logwin(self):
        self.ui.plainTextEdit.clear()

    def clear_field(self):
        self.ui.statusBar.showMessage('')
        self.ui.groupBox_3.setTitle('')
        self.ui.lecontest.setText('')
        self.ui.plainTextEdit.clear()
        self.centralWidget().repaint()

    @QtCore.pyqtSlot()
    def pb_save_report(self):
        if self.ui.plainTextEdit.toPlainText() == '':
            self.ui.statusBar.showMessage(self.tr('No information to save'))
            return
        self.save_report(None)

    def save_report(self, file):
        if file is None:
            selected_file, _ = QFileDialog.getSaveFileName(self, self.tr("Save output to file:"),
                                                           CONTEST_PATH, "*{0}".format(extension),
                                                           options=QFileDialog.DontUseNativeDialog)
            if not QtCore.QFileInfo(selected_file).suffix():
                file = selected_file + extension
            else:
                file = selected_file
        else:
            file += extension
        self.ui.statusBar.showMessage(self.tr('Report saved as ') + '{0}'.format(file))
        with open(file, 'w') as report:
            report.write(str(self.ui.plainTextEdit.toPlainText()))

    @staticmethod
    def count():
        os.chdir(CONTEST_PATH)
        num = str(len(list(Path('.').glob('**/*'))) - len(list(Path('.').glob('*'))))
        return num


def read_config():
    global settings, language, CONTEST_PATH
    settings = QtCore.QSettings('ut2vr', 'LogChecker')
    language = settings.value("language")
    if not language:
        language = QtCore.QLocale.system().name()
    CONTEST_PATH = settings.value("Contests_path", os.path.join(os.path.expanduser('~'), 'Contests'))


def main():
    global command, rules, settings
    global language, checker
    app = QtWidgets.QApplication([])

    read_config()
    lang_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Languages")
    lang_file = "{}.qm".format(language)
    app_translator = QTranslator(app)
    # qt_translator = QTranslator(app)
    # qt_lang_path = QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath)
    # qt_lang_file = "{}.qm".format(language)
    app_translator.load(lang_file, lang_path)
    # qt_translator.load(qt_lang_file, qt_lang_path)
    # app.installTranslator(qt_translator)
    app.installTranslator(app_translator)

    application = mywindow()
    application.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    sys.exit(main())
