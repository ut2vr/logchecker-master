<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../LogXchecker_GUI.py" line="171"/>
        <source>logXcheckerGUI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="183"/>
        <source>Contest</source>
        <translation>Контест</translation>
    </message>
    <message>
        <location filename="LogXchecker_GUI.py" line="171"/>
        <source>Checking result</source>
        <translation type="obsolete">Результат проверки</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="173"/>
        <source>Criteria</source>
        <translation>Соответствие</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="174"/>
        <source>edi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="175"/>
        <source>Rules</source>
        <translation>правила</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="176"/>
        <source>Out format</source>
        <translation>Вывод</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="177"/>
        <source>txt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="178"/>
        <source>json</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="179"/>
        <source>XML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="180"/>
        <source>csv</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="181"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="182"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="184"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="185"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="186"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="187"/>
        <source>Open contest</source>
        <translation>Открыть тест</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="188"/>
        <source>Open log</source>
        <translation>Открыть лог</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="189"/>
        <source>Open rules</source>
        <translation>Открыть правила</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="190"/>
        <source>Create new contest</source>
        <translation>Создать новый тест</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="191"/>
        <source>Cross check contest</source>
        <translation>Перекрестная проверка</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="192"/>
        <source>Compliance check</source>
        <translation>Проверка на соответствие</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="193"/>
        <source>Select language</source>
        <translation>Выбрать язык</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="194"/>
        <source>Archive the contest </source>
        <translation>Архивировать тест </translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="195"/>
        <source>Restore the contest</source>
        <translation>Восстановить тест</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="196"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../LogXchecker_GUI.py" line="197"/>
        <source>Read me</source>
        <translation>Справка</translation>
    </message>
</context>
<context>
    <name>mywindow</name>
    <message>
        <location filename="../LogChecker.py" line="76"/>
        <source>Select rules file:</source>
        <translation>Выберите файл правил:</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="79"/>
        <source>Rules file not selected</source>
        <translation>Файл правил не выбран</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="101"/>
        <source>Not yet implemented</source>
        <translation>Пока не реализовано</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="116"/>
        <source>GUI for logXchecker by ut2vr 2021</source>
        <translation></translation>
    </message>
    <message>
        <location filename="LogCheck.py" line="112"/>
        <source>edi format compliance check:
</source>
        <translation type="obsolete">Проверка на соответствие формату edi:
</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="138"/>
        <source>edi format compliance check:</source>
        <translation>Проверка на соответствие формату edi:</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="147"/>
        <source>Select contest folder:</source>
        <translation>Выберите папку теста:</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="150"/>
        <source>Folder not selected</source>
        <translation>Папка не выбрана</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="156"/>
        <source>Contest folder does not contain a file with the correct test name</source>
        <translation>Папка теста не содержит файла с правильный именем теста</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="160"/>
        <source>Folder name and contest name in rules file do not match</source>
        <translation>Имя папки теста не совпадает с именем теста в файле правил</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="163"/>
        <source>Contest&apos;s file ready to check</source>
        <translation>Файлы теста готовы к проверке</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="201"/>
        <source>Please, wait...</source>
        <translation>Минутку...</translation>
    </message>
    <message>
        <location filename="LogCheck.py" line="144"/>
        <source>logs processed</source>
        <translation type="obsolete">логов обработано</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="188"/>
        <source>Score and UBN files are saved in the contest folder</source>
        <translation>Файлы Score и UBN сохранены в папке теста</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="192"/>
        <source>First define the folder and contest rules</source>
        <translation>Прежде определите папку и правила теста</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="206"/>
        <source>Select log folder:</source>
        <translation>Выберите папку с логами:</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="209"/>
        <source>Folder with logs not selected</source>
        <translation>Папка с логами не выбрана</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="230"/>
        <source>Select log file:</source>
        <translation>Выберите файл лога:</translation>
    </message>
    <message>
        <location filename="LogCheck.py" line="214"/>
        <source>Checked for compliance with the rules of the contest - </source>
        <translation type="obsolete">Проверено на соответствие правилам теста - </translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="273"/>
        <source>No information to save</source>
        <translation>Нет информации для сохранения</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="280"/>
        <source>Save output to file:</source>
        <translation>Сохранить вывод в файл:</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="288"/>
        <source>Report saved as </source>
        <translation>Отчет сохранен как </translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="141"/>
        <source>rules format compliance check:</source>
        <translation>Проверка на соответствие правилам теста:</translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="173"/>
        <source>logs processed - </source>
        <translation>Обработано логов - </translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="243"/>
        <source>Rules in force - </source>
        <translation>Применены правила - </translation>
    </message>
    <message>
        <location filename="../LogChecker.py" line="41"/>
        <source>&quot;logXchecker.py&quot; file not found</source>
        <translation></translation>
    </message>
</context>
</TS>
