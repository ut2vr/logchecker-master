LogChecker_GUI
=================

LogXchecker graphical user interface

git clone https://ut2vr@bitbucket.org/ut2vr/logchecker.git

Screenshots
-----------

Requirements
------------

-coverage==4.2
-dicttoxml==1.7.4
-nose==1.3.7
-validate-email==1.3
-PyQt5>=5.14.2


Installation
------------
Ubuntu:
*******


Поместить архив в любую директорию,
распаковать,
запустить в терминале следующие команды:

$ pip3 install .	# Обратить внимание на точку
$ set_contests.sh

Windows:
********

Пока не реализовано

Usage
_____

logcheker


Todo:
-----

- архивирование директории контеста
- извлечение из архива директории контеста
- создание нового контеста
- редактирование файла правил


